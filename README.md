**Demo application for powerhouse**

You should have PostgreSql Server running and specify the  port described in _application.properties_ file

Also you should have installed Java 8 and maven 
You can import project to IDE or build it from terminal 

You should build the project with maven(tests also will be executed) :
_mvn clean install_ 

 and then run command to start the project:
_java -jar -DSpring.profiles.active=local  demo-0.0.1-SNAPSHOT.jar_

See REST api document for description api:
_http://localhost:8080/swagger-ui.html_

See postman collection in project as example of JSON and test data




