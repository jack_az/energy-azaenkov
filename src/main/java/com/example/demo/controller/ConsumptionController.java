package com.example.demo.controller;

import com.example.demo.service.ConsumptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Month;

@RestController
@RequestMapping("api/consumption")
@Api(value="Consumption Controller ", description="Operation to get consumption value for specific meter")
public class ConsumptionController {

    private final ConsumptionService consumptionService;

    public ConsumptionController(ConsumptionService consumptionService) {
        this.consumptionService = consumptionService;
    }

    @ApiOperation(value = "Get consumption for month")
    @GetMapping(value = "/{profileId}/{month}/{meterId}")
    public ResponseEntity<?> getConsumption(@PathVariable("profileId") Long profileId,
                                            @PathVariable("month") Month month,
                                            @PathVariable("meterId") Long meterId
    ) {
        Integer consumptionForMonth = consumptionService.getConsumptionForMonth(profileId, month, meterId);
        if (consumptionForMonth == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.ok(consumptionForMonth);
    }
}
