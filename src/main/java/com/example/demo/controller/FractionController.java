package com.example.demo.controller;

import com.example.demo.model.Profile;
import com.example.demo.service.FractionService;
import com.example.demo.service.ValidationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/fraction")
@Api(value = "Fraction Controller", description = "CRUD operations for fractions")
public class FractionController {

    private final FractionService fractionService;

    private final ValidationService validationService;

    public FractionController(FractionService fractionService, ValidationService validationService) {
        this.fractionService = fractionService;
        this.validationService = validationService;
    }

    @ApiOperation(value = "Add profile with fractions for year")
    @PostMapping(value = "/add",
            consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody Profile profile) {
        if (!validationService.isProfileFractionsValid(profile)) {
            return ResponseEntity.status(400).build();
        }
        fractionService.saveProfile(profile);
        return ResponseEntity.status(201).build();
    }

    @ApiOperation(value = "Get fractions by Profile ID")
    @GetMapping(value = "/{profileId}",
            produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProfile(@PathVariable("profileId") Long profileId) {
        Profile profile = fractionService.findById(profileId);
        if (profile == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.ok(profile);
    }

    @ApiOperation(value = "Update fractions by Profile ID")
    @PutMapping(value = "/{profileId}",
            consumes = MimeTypeUtils.APPLICATION_JSON_VALUE,
            produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateProfile(@PathVariable("profileId") Long profileId,
                                           @RequestBody Profile profileForUpdate) {

        Profile currentProfile = fractionService.findById(profileId);
        if (currentProfile == null || !validationService.isProfileFractionsValid(profileForUpdate)) {
            return ResponseEntity.status(404).build();
        }
        fractionService.update(currentProfile,profileForUpdate);
        return ResponseEntity.status(200).build();
    }


    @ApiOperation(value = "Delete fractions by Profile ID")
    @DeleteMapping(value = "/{profileId}",
            consumes = MimeTypeUtils.APPLICATION_JSON_VALUE,
            produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProfile(@PathVariable("profileId") Long profileId) {
        Profile profile = fractionService.findById(profileId);
        if (profile != null) {
            fractionService.deleteByProfileId(profileId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(404).build();
    }
}
