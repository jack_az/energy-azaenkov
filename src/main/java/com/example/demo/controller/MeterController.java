package com.example.demo.controller;

import com.example.demo.dto.MeterResponce;
import com.example.demo.model.Meter;
import com.example.demo.model.MeterWrapper;
import com.example.demo.model.Profile;
import com.example.demo.service.FractionService;
import com.example.demo.service.MeterService;
import com.example.demo.service.ValidationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/meter")
@Api(value = "Meter Controller", description = "CRUD operations for Meter")
public class MeterController {

    private final MeterService meterService;

    private final FractionService fractionService;

    private final ValidationService validationService;


    public MeterController(MeterService meterService, FractionService fractionService, ValidationService validationService) {
        this.meterService = meterService;
        this.fractionService = fractionService;
        this.validationService = validationService;
    }


    @ApiOperation(value = "Add meter list for year with specific profile and meter ID")
    @PostMapping(value = "/add",
            consumes = MimeTypeUtils.APPLICATION_JSON_VALUE,
            produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody MeterWrapper meterWrapper) {
        if (!validationService.meterReadingValidation(meterWrapper.getMeterList()) &&
                !validationService.isConsumptionValid(meterWrapper)) {
            return ResponseEntity.status(400).build();
        }
        for (Meter m : meterWrapper.getMeterList()) {
            Profile profile = fractionService.findById(m.getProfile().getId());
            if (profile != null) {
                m.setProfile(profile);
            } else return ResponseEntity.status(404).body(m.getProfile().getName());
        }
        meterService.save(meterWrapper.getMeterList());
        return ResponseEntity.status(201).build();
    }

    @ApiOperation(value = "Get meter list for year for specific meter ID")
    @GetMapping(value = "/{id}",
            produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getMeter(@PathVariable("id") Long id) {
        List<Meter> meter = meterService.findByMeterId(id);
        if (meter == null) {
            return ResponseEntity.status(404).build();
        }
        List<MeterResponce> converter = new ArrayList<>();
        meter.forEach(m -> converter.add(
                new MeterResponce(m.getMonth(), m.getValue())));
        return ResponseEntity.ok(converter);
    }


    @ApiOperation(value = "Delete meter list for year for specific meter ID")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteMeter(@PathVariable("id") Long id) {
        List<Meter> meter = meterService.findByMeterId(id);
        if (meter != null) {
            meterService.deleteByMeterId(id);
            return ResponseEntity.ok().build();
        } else return ResponseEntity.status(404).build();
    }


    @ApiOperation(value = "Update meter list for year for specific profile ID")
    @PutMapping(value = "/{profileId}",
            consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateMeter(@PathVariable("profileId") Long profileId,
                                         @RequestBody MeterWrapper meterWrapper) {
        if (!validationService.meterReadingValidation(meterWrapper.getMeterList()) &&
                !validationService.isConsumptionValid(meterWrapper)) {
            return ResponseEntity.status(400).build();
        }
        Profile profile = fractionService.findById(profileId);
        if (profile == null) {
            return ResponseEntity.status(404).build();
        }
        meterService.update(meterWrapper, profileId);
        return ResponseEntity.status(200).build();
    }
}
