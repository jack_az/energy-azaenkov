package com.example.demo.dto;

import java.time.Month;

public class MeterResponce {

    private final Month month;
    private final Integer value;

    public MeterResponce(Month month, Integer value) {
        this.month=month;
        this.value=value;
    }

    public Month getMonth() {
        return month;
    }

    public Integer getValue() {
        return value;
    }
}
