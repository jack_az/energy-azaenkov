package com.example.demo.model;


import javax.persistence.*;
import java.time.Month;

@Entity
@Table(name = "fraction")
public class Fraction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "month")
    private Month month;

    @Column(name = "fraction_value")
    private Double fractionValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public Double getFractionValue() {
        return fractionValue;
    }

    public void setFractionValue(Double fractionValue) {
        this.fractionValue = fractionValue;
    }

}
