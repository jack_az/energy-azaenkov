package com.example.demo.model;

import java.util.List;

public class MeterWrapper {
    private List<Meter> meterList;

    public List<Meter> getMeterList() {
        return meterList;
    }

    public void setMeterList(List<Meter> meterList) {
        this.meterList = meterList;
    }
}
