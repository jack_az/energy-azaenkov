package com.example.demo.repository;

import com.example.demo.model.Meter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeterRepository extends CrudRepository<Meter, Long> {

    List<Meter> findByMeterId (Long id);

    List<Meter> getMeterByProfileId (Long profileId);

    void deleteByMeterId (Long meterId);
}
