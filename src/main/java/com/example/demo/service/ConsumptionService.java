package com.example.demo.service;

import java.time.Month;

public interface ConsumptionService {

    Integer getConsumptionForMonth(Long profileId, Month month, Long meterId);
}
