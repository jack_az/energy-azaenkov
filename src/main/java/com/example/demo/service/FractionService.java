package com.example.demo.service;

import com.example.demo.model.Profile;

public interface FractionService {

    void saveProfile(Profile profile);

    Profile findById(Long id);

    void update(Profile currentProfile, Profile profileForUpdate);

    void deleteByProfileId(Long id);

}
