package com.example.demo.service;

import com.example.demo.model.Meter;
import com.example.demo.model.MeterWrapper;

import java.util.List;

public interface MeterService {

    List<Meter> findByMeterId(Long id);

    void deleteByMeterId(Long id);

    void update(MeterWrapper meterWrapper, Long profileId);

    void save(List<Meter> meterList);
}
