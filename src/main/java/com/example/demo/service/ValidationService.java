package com.example.demo.service;

import com.example.demo.model.Meter;
import com.example.demo.model.MeterWrapper;
import com.example.demo.model.Profile;

import java.util.List;

public interface ValidationService {

    boolean isProfileFractionsValid(Profile profile);

    boolean isConsumptionValid(MeterWrapper meterWrapper);

    boolean meterReadingValidation(List<Meter> meterList);
}
