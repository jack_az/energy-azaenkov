package com.example.demo.service.impl;

import com.example.demo.model.Meter;
import com.example.demo.repository.MeterRepository;
import com.example.demo.service.ConsumptionService;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.List;

@Service
public class ConsumptionServiceImpl implements ConsumptionService {

    private final MeterRepository meterRepository;

    public ConsumptionServiceImpl(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }


    @Override
    public Integer getConsumptionForMonth(Long profileId, Month month, Long meterId) {
        int currentMonthValue = getValueForMonth(profileId, month, meterId);
        int currentMonthNumber = month.getValue();
        if (month.equals(Month.JANUARY)) {
            return currentMonthValue;
        }
        Month previousMonth = Month.of(currentMonthNumber - 1);
        int previousMonthValue = getValueForMonth(profileId, previousMonth, meterId);
        return currentMonthValue - previousMonthValue;
    }

    private Integer getValueForMonth(Long id, Month month, Long meterId) {
        List<Meter> list = meterRepository.getMeterByProfileId(id);
        return list.stream()
                .filter(f -> f.getMeterId().equals(meterId))
                .filter(f -> f.getMonth().equals(month))
                .findFirst()
                .map(Meter::getValue).orElse(0);

    }
}
