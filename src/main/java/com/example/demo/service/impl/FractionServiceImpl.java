package com.example.demo.service.impl;

import com.example.demo.model.Profile;
import com.example.demo.repository.ProfileRepository;
import com.example.demo.service.FractionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FractionServiceImpl implements FractionService {

    public FractionServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    private final ProfileRepository profileRepository;

    public void saveProfile(Profile profile) {
        profileRepository.save(profile);
    }

    public Profile findById(Long id) {
        return profileRepository.findOne(id);
    }

    public void update(Profile currentProfile, Profile profileForUpdate) {
        currentProfile.getFractionList().forEach(fraction -> profileForUpdate.getFractionList().forEach(fraction1 -> {
            if (fraction.getMonth().equals(fraction1.getMonth())) {
                fraction.setFractionValue(fraction1.getFractionValue());
            }
        }));
    }

    public void deleteByProfileId(Long id) {
        profileRepository.delete(id);
    }

}
