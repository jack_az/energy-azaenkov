package com.example.demo.service.impl;

import com.example.demo.model.Meter;
import com.example.demo.model.MeterWrapper;
import com.example.demo.repository.MeterRepository;
import com.example.demo.service.MeterService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class MeterServiceImpl implements MeterService {

    private final MeterRepository meterRepository;

    public MeterServiceImpl(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }

    public List<Meter> findByMeterId(Long id) {
        return meterRepository.findByMeterId(id);
    }

    public void deleteByMeterId(Long id) {
        meterRepository.deleteByMeterId(id);
    }

    public void update(MeterWrapper meterWrapper, Long profileId) {
        meterWrapper.getMeterList().forEach(meterForUpdate -> {
            List<Meter> current = meterRepository.getMeterByProfileId(profileId);
            if (current != null && !current.isEmpty()) {
                current.forEach(currentMeter -> {
                    if (meterForUpdate.getMonth().equals(currentMeter.getMonth())) {
                        currentMeter.setValue(meterForUpdate.getValue());
                    }
                });
            }
        });
    }

    public void save(List<Meter> meterList) {
        meterRepository.save(meterList);
    }

}
