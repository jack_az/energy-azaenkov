package com.example.demo.service.impl;

import com.example.demo.model.Fraction;
import com.example.demo.model.Meter;
import com.example.demo.model.MeterWrapper;
import com.example.demo.model.Profile;
import com.example.demo.repository.ProfileRepository;
import com.example.demo.service.ValidationService;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.List;

@Service
public class ValidationServiceImpl implements ValidationService {

    private static final double consumptionTolerance = 0.25;


    private final ProfileRepository profileRepository;


    public ValidationServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }


    @Override
    public boolean isProfileFractionsValid(Profile profile) {
        double sumTransactions = profile.getFractionList()
                .stream()
                .mapToDouble(Fraction::getFractionValue)
                .sum();
        return sumTransactions == 1;
    }

    @Override
    public boolean isConsumptionValid(MeterWrapper meterWrapper) {
        Long profileId = meterWrapper.getMeterList().get(0).getProfile().getId();
        Profile profile = profileRepository.findOne(profileId);
        for (Meter m : meterWrapper.getMeterList()) {
            if (!isConsumptionForCurrentMonthValid(profile.getFractionList(), meterWrapper.getMeterList(), m.getMonth())) {
                return false;
            }
        }
        return true;
    }

    private boolean isConsumptionForCurrentMonthValid(List<Fraction> fractionList, List<Meter> meterList, Month month) {
        int consumptionForMonth = getConsumptionForCurrentMonth(meterList, month);
        double fractionForCurrentMonth = getFractionForCurrentMonth(fractionList, month);
        int totalForYear = getTotalForYear(meterList);
        int currentValue = (int) (totalForYear * fractionForCurrentMonth);
        int upperValueWithTolerance = (int) (currentValue + currentValue * consumptionTolerance);
        int lowerValueWithTolerance = (int) (currentValue - currentValue * consumptionTolerance);
        return consumptionForMonth >= lowerValueWithTolerance && consumptionForMonth <= upperValueWithTolerance;

    }

    private Double getFractionForCurrentMonth(List<Fraction> fractionList, Month month) {
        return fractionList.stream()
                .filter(f -> f.getMonth().equals(month))
                .findFirst()
                .map(Fraction::getFractionValue)
                .orElse(0.0);
    }

    private Integer getTotalForYear(List<Meter> meterList) {
        return meterList.stream()
                .filter(f -> f.getMonth().equals(Month.DECEMBER))
                .findFirst()
                .map(Meter::getValue)
                .orElse(0);
    }

    private Integer getConsumptionForCurrentMonth(List<Meter> meterList, Month month) {
        int valueForMonth = getValueForMonth(meterList, month);
        int currentMonthNumber = month.getValue();
        if (month.equals(Month.JANUARY)) {
            return valueForMonth;
        }
        Month previousMonth = Month.of(currentMonthNumber - 1);
        int valueForPreviousMonth = getValueForMonth(meterList, previousMonth);
        return valueForMonth - valueForPreviousMonth;
    }

    private Integer getValueForMonth(List<Meter> meterList, Month month) {
        return meterList.stream()
                .filter(f -> f.getMonth().equals(month))
                .findFirst()
                .map(Meter::getValue).orElse(0);
    }

    @Override
    public boolean meterReadingValidation(List<Meter> meterList) {
        for (int i = 0; i < meterList.size() - 1; i++) {
            if (meterList.get(i + 1).getValue() < meterList.get(i).getValue()) {
                return false;
            }
        }
        return true;
    }
}



