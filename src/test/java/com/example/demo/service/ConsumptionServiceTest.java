package com.example.demo.service;


import com.example.demo.repository.MeterRepository;
import com.example.demo.service.impl.ConsumptionServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Month;

import static com.example.demo.util.TestDataUtil.getValidMeterList;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConsumptionServiceTest {

    @Mock
    private MeterRepository meterRepository;
    @InjectMocks
    private ConsumptionServiceImpl consumptionService;

    @Before
    public void setUp() {
        when(meterRepository.getMeterByProfileId(1L))
                .thenReturn(getValidMeterList())
                .thenReturn(getValidMeterList());
    }

    @Test
    public void getConsumptionForMonthValid() {
        int consumption = consumptionService.getConsumptionForMonth(1L, Month.MARCH, 1L);
        Assert.assertEquals(10, consumption);
    }

    @Test
    public void getConsumptionForMonthJanuaryValid() {
        int consumption = consumptionService.getConsumptionForMonth(1L, Month.JANUARY, 1L);
        Assert.assertEquals(10, consumption);
    }


}
