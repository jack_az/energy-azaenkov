package com.example.demo.service;

import com.example.demo.repository.ProfileRepository;
import com.example.demo.service.impl.ValidationServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.example.demo.util.TestDataUtil.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest {

    @Mock
    private ProfileRepository profileRepository;
    @InjectMocks
    private ValidationServiceImpl validationService;

    @Before
    public void setUp() {
        when(profileRepository.findOne(1L))
                .thenReturn(getProfileWithValidFractions())
                .thenReturn(getProfileWithInValidFractions());
    }

    @Test
    public void meterReadingValidData() {
        boolean expectedTrue = validationService.meterReadingValidation(getValidMeterList());
        Assert.assertEquals(expectedTrue, true);
    }

    @Test
    public void meterReadingInvalidData() {
        boolean expectedFalse = validationService.meterReadingValidation(getInValidMeterList());
        Assert.assertEquals(expectedFalse, false);
    }

    @Test
    public void profileWithValidFractionsData() {
        boolean expectedTrue = validationService.isProfileFractionsValid(getProfileWithValidFractions());
        Assert.assertEquals(expectedTrue, true);
    }

    @Test
    public void profileWithInvalidFractionsData() {
        boolean expectedFalse = validationService.isProfileFractionsValid(getProfileWithInValidFractions());
        Assert.assertEquals(expectedFalse, false);
    }

    @Test
    public void consumptionValidData() {
        boolean expectedTrue = validationService.isConsumptionValid(getMeterWrapperWithValidConsamption());
        Assert.assertEquals(expectedTrue, true);
    }

    @Test
    public void consumptionInvalidData() {
        boolean expectedFalse = validationService.isConsumptionValid(getMeterWrapperWithInvalidConsamption());
        Assert.assertEquals(expectedFalse, false);
    }
}
